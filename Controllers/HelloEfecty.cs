﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TestAPIEfecty.Controllers
{
    [ApiController]
    [Route("/api/efecty")]
    public class HelloEfecty : ControllerBase
    {
        [HttpGet]
        public String Get()
        {
            return "Hola Mundo Efecty";
        }
    }
}
